import React from 'react';
import Carousel from 'react-bootstrap/Carousel'
import 'pure-react-carousel/dist/react-carousel.es.css';
/*import Banner from './../components/Banner'*/
import Highlights from './../components/Highlights';
import Footer from './../components/Footer';

export default function Home() {
	return(
		<div>
		<Carousel>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>SHOP EASILY</h3>
		    </Carousel.Caption>
		    <img 
		      className="d-block w-100"
		      src="https://www.kindpng.com/picc/m/161-1619663_ecommerce-website-images-with-transparent-background-hd-png.png"
		      alt="First slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>ONLINE SHOPPING FRIENDLY</h3>
		    </Carousel.Caption>
		    <img
		      className="d-block w-100"
		      src="https://cdn.searchenginejournal.com/wp-content/uploads/2016/12/Depositphotos_31395529_l-2015.jpg"
		      alt="Second slide"
		    />

		  </Carousel.Item>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>HUSTLE FREE</h3>
		    </Carousel.Caption>
		    <img
		      className="d-block w-100 active"
		      src="http://il6.picdn.net/shutterstock/videos/10231016/thumb/12.jpg"
		      alt="Third slide"
		    />

		  </Carousel.Item>
		</Carousel>
{/*
			<Banner bannerData={data}/>*/}
			<Highlights />
			<Footer />
		</div>
		);
};